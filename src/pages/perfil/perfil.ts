import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Platform, ActionSheetController } from 'ionic-angular';
import { PersonProvider } from '../../providers/person/person';
import { PersonDetail } from '../../models/PersonDetail';

import { UtilsProvider } from '../../providers/utils/utils';
import { Camera } from '@ionic-native/camera';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { Events } from 'ionic-angular/util/events';


@IonicPage({
  name: 'perfil',
  segment: 'perfil'
})
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  user: PersonDetail;
  estados = new Array();
  cidades: any;
  ImagemBase64: any;

  tab = "meus-dados";

  selectOptEstados = {
    title: 'Selecione o estado',
    subTitle: 'Selecione o estado',
    checked: true
  }

  selectOptCidades = {
    title: 'Selecione a cidade',
    subTitle: 'Selecione a cidade',
    checked: true
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private person: PersonProvider,
    public alertCtrl: AlertController,
    public utils: UtilsProvider,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public events: Events,
  ) {
    this.user = new PersonDetail();
    this.getEstados();
    this.getPersonDetail();

    if (this.user.Estado != '' && this.user.Estado != null) {
      this.selectOptEstados.checked = false;
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PerfilPage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getPersonDetail()

      refresher.complete();
    }, 1000);

  }
  getPersonDetail() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.person.get()
      .then((response: Object) => {
        loading.dismiss();
        this.user = new PersonDetail().fromJSON(response);
        if (this.user.Estado != '' && this.user.Estado != null) {
          this.onSelectCidade(this.user.Estado);
        }
      }, (error) => {
        loading.dismiss();
        console.log(error);
        this.utils.showAlert('ocorreu um erro. Tente novamente mais tarde.');
      });
  }

  onSelectCidade(uf) {
    this.utils.getCidades(uf).then((result: any) => { this.cidades = result; }, (error) => { });

    if (this.user.Cidade != '' && this.user.Cidade != null) {
      this.selectOptCidades.checked = false;
    }
  }

  getEstados() {
    this.utils.getEstados().then((result: any) => { this.estados = result; }, (error) => { });


  }

  putPerson() {
    let loading = this.loadingCtrl.create({
      content: 'Salvando...'
    });
    loading.present();

    this.person.put(this.user)
      .then((response: any) => {
        // loading.dismiss();
        // setTimeout(() => {
          loading.dismiss();
          console.log(response);
          
          this.person.get()
          .then((response: any) => {
            this.events.publish('profile', response);
          });


          this.utils.showModalSucesso(response.Titulo, response.Mensagem, "OK", null);
        // }, 1000);
        // setTimeout(() => {
        //   this.navCtrl.setRoot(HomePage);
        //           }, 3000);

      }, (error) => {
        loading.dismiss();
        console.log(error);
        this.utils.showAlert(error.error.Message);
      });
  }

  putFoto(image) {
    let loading = this.loadingCtrl.create({
      content: 'Salvando...'
    });
    loading.present();

    this.person.putFoto(image)
      .then((response: any) => {
       // loading.dismiss();

        this.user.CaminhoFoto = response.Url;

        this.person.get()
        .then((profile: any) => {
          loading.dismiss();
          this.events.publish('profile', profile);
          this.utils.showModalSucesso(response.Titulo, response.Mensagem, "OK", null);
        });
      }, (error) => {
        loading.dismiss();
        console.log(error);
        // this.utils.showAlert(error.error.Message);
      });
  }


  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Escolha uma imagem',
      buttons: [
        {
          text: 'Abrir Galeria',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar a Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 75,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
    };
    this.camera.getPicture(options).then((imagePath) => {
      this.putFoto(imagePath);
    }, (err) => {
      // this.presentToast('Erro ao selecionar a imagem');
    });
  }
}




