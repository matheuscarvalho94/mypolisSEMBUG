import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParticipacaoParticipacaoDetalhePage } from './participacao-participacao-detalhe';

@NgModule({
  declarations: [
    ParticipacaoParticipacaoDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(ParticipacaoParticipacaoDetalhePage),
  ],
})
export class ParticipacaoParticipacaoDetalhePageModule {}
