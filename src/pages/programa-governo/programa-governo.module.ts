import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgramaGovernoPage } from './programa-governo';
import { ConvideAmigosPage } from './convide-amigos';

@NgModule({
  declarations: [
    ProgramaGovernoPage,
    ConvideAmigosPage
  ],
  imports: [
    IonicPageModule.forChild(ProgramaGovernoPage),
    IonicPageModule.forChild(ConvideAmigosPage)
  ],
})
export class ProgramaGovernoPageModule {}
