import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  hiddenNext:boolean = false

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {}

  goState() {

    this.navCtrl.setRoot(LoginPage) 
    
  }


  goToSlide() {

    this.slides.slideNext()
  }

  slideChanged() {

    this.slides.isEnd() == true ? this.hiddenNext = true : this.hiddenNext = false

  }
  
}
