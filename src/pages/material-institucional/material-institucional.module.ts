import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterialInstitucionalPage } from './material-institucional';

@NgModule({
  declarations: [
    MaterialInstitucionalPage,
  ],
  imports: [
    IonicPageModule.forChild(MaterialInstitucionalPage),
  ],
})
export class MaterialInstitucionalPageModule {}
