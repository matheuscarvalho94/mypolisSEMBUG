import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroTipoPerfilPage } from './cadastro-tipoperfil';
import { BrMaskerModule } from 'brmasker-ionic-3';



@NgModule({
  declarations: [
    CadastroTipoPerfilPage
  ],
  imports: [
    IonicPageModule.forChild(CadastroTipoPerfilPage),
    BrMaskerModule

  ],
})
export class CadastroTipoPerfilPageModule {}
