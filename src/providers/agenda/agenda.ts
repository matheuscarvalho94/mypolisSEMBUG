import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';

@Injectable()
export class AgendaProvider {

  constructor(public http: HttpClient) {
    
  }

  getLista(token:string, pag:number, size:number) {

    let url = `${CONFIG_PROJECT.baseApi}/Agenda?Page=${pag}&Size=${size}`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  getListaSemPaginar(token:string) {
    
        let url = `${CONFIG_PROJECT.baseApi}/Agenda?$Paginar=${false}`;
        let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };
    
        return new Promise((resolve, reject) => {
          this.http.get(url, header)
            .map(res => res)
            .subscribe((result: any) => {
              resolve(result);
            },
            (error) => {
              reject(error);
            });
        });
      }

  filterCity(token:string, filter, pag:number, skip:number) {

    let url = `${CONFIG_PROJECT.baseApi}/Agenda?Paginar=`+ false+ `$skip=${skip}&$top=${pag}&$filter=Local eq '${filter.cidade + ', ' + filter.estado}'`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

  }

  getDetalhe(token:string,id:any)
  {
    let url = `${CONFIG_PROJECT.baseApi}/Agenda/` + id;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  filterData(token:string, filtro:any) {

    let url = `${CONFIG_PROJECT.baseApi}/Agenda/Filtro/` + filtro;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

  }
}
