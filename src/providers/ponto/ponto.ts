import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class PontoProvider {

  constructor(public http: HttpClient, public auth: AuthProvider) {}

  getExtrato()
  {
    let url = `${CONFIG_PROJECT.baseApi}/Pontos/Extrato`;
    var token = this.auth.getToken();
    let header = { "headers": { "authorization": 'bearer ' + token } };
 
    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  getMedalhas()
  {
    let url = `${CONFIG_PROJECT.baseApi}/Pontos/Medalhas`;
    var token = this.auth.getToken();
    let header = { "headers": { "authorization": 'bearer ' + token } };
 
    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }
}